/* eslint-disable */
import { FunctionComponent } from '@tarojs/taro';

interface Props {
  name: 'game-----' | 'ios-home' | 'md-people' | 'md-remove' | 'md-redo' | 'md-save' | 'md-undo' | 'ios-add' | 'ios-add-circle-outline' | 'ios-checkmark' | 'ios-checkmark-circle-outline' | 'ios-close' | 'ios-close-circle-outline' | 'ios-create' | 'ios-options' | 'ios-pie' | 'ios-send' | 'ios-settings' | 'ios-trash';
  size?: number;
  color?: string | string[];
}

export declare const IconFont: FunctionComponent<Props>;

export default IconFont;
