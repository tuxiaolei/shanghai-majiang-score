import Taro from '@tarojs/taro'
import { View } from '@tarojs/components'
import { ClCard, ClFlex, ClLayout, ClText } from 'mp-colorui'
import classNames from 'classnames'

import './index.scss'

export default function GameInfoCard(props){

    const handleClick = () => {
        // 如果是列表，那么就响应点击事件，跳转到帖子详情
        if (props.isList) {
          const { gameTitle, content } = this.props
          Taro.navigateTo({
            url: `/pages/gameRound/post?title=${gameTitle}&content=${content}`,
          })
        }
      }

      return (

        <ClCard 
          active 
          onClick={handleClick}
          className={classNames('gameinfoCard', { postcard__isList: props.isList })}
        >
        <ClFlex>
        <View className='flex-sub'>
            <ClText textColor='blue' className='game-title'>{props.gameTitle}</ClText>
        </View>
        <View className='flex-sub' >
        <ClText textColor='blue' className='game-date'>{props.gameDate}</ClText>
        </View>
        </ClFlex>
        <ClFlex>
        <View className='flex-sub'>
            <ClText textColor='blue' className='game-type'>{props.gameType}</ClText>
        </View>
        <View className='flex-sub'>
            <ClText textColor='blue' className='game-count'>{props.gameCount}</ClText>
        </View>
        <View className='flex-sub' >
        <ClText textColor='blue' className='game-hours'>{props.gameHours}</ClText>
        </View>
        </ClFlex>
      </ClCard>
      )
    }

    GameInfoCard.defaultProps = {
        isList: '',




}