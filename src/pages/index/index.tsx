import Taro, { useState } from "@tarojs/taro";
import { View, Text } from "@tarojs/components";
import { AtFab, AtFloatLayout, AtMessage } from "taro-ui";
import { ClFloatButton } from "mp-colorui";
import { PostCard, PostForm } from "../../components";

import "./index.scss";

export default function Index() {
  const [posts, setPosts] = useState([
    {
      title: '',
      content: ''
    }
  ]);


  const [formTitle, setFormTitle] = useState("");
  const [formContent, setFormContent] = useState("");
  const [isOpened, setIsOpened] = useState(false);

  const handleCreate=()=>{

    Taro.navigateTo({
      url: `/pages/gameSet/gameSet`,
    });

  //  Taro.atMessage({
   //   message: "创造新游戏",
   //   type: "success"
   // });
  }

  return (
    <View className='index'>
      <AtMessage />
      {posts.map((post) => (
        <PostCard
          key={post.id}
          title={post.title}
          content={post.content}
          isList
        />
      ))}
  
      <View className='post-button'>
        <ClFloatButton 
          onClick={handleCreate} 
          size='large'
          bgColor='red'
        >
          <Text className='at-fab__icon at-icon at-icon-edit'></Text>
        </ClFloatButton>
      </View>
    </View>
  );
}

Index.config = {
  navigationBarTitleText: "首页"
};
