import Taro,{useState} from "@tarojs/taro";
import { View,Text } from "@tarojs/components";
import { AtCard, AtInput, AtForm, AtButton, AtDrawer } from "taro-ui"
import bgcolorlist from "../../components/color";

import "./gameSet.scss";

export default function GameSet() {
  const rules = {
    name(rule, value, callback) {
      if (!rule.required(value)) {
        callback("姓名不能为空");
        return false;
      }
    }
  };
  const [right, setRight] = useState(false);
  const [bgcard_1, setBgcard_1] = useState('green');
  const [bgcard_2, setBgcard_2] = useState('orange');
  const [bgcard_3, setBgcard_3] = useState('olive');
  const [bgcard_4, setBgcard_4] = useState('cyan');
  const ll = bgcolorlist;
  console.log(ll)
  return (
    <View>
      <AtCard background-color={bgcard_1}>
        <View className='at-row'>
          <View className='flex-sub'>
          <Text foreground-color='white' font-size='16' >
            东 ：
          </Text>
          </View>

          <View className='flex-twice'>
          <AtInput placeholder='玩家一' autoFocus clear maxLength='5'  />
          </View>
          <View className='flex-twice'>
          <AtButton className='cu-btn'  background-color='white'>blue</AtButton>
          </View>
          
        </View>

      </AtCard >

      <AtDrawer
        show={right}
        direction='right'
        onCancel={() => {
          setRight(false);
        }}
      >
        我是右侧抽屉
</AtDrawer>
    </View>
  );
}

GameSet.config = {
  navigationBarTitleText: "设置"
};
